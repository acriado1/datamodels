# Contributing

This repository follows a Git flow based on main and auxiliary branches as well as versioning with tags.

- **Main Branches**
    - **master**: branch where develop and hotfix branches are merged and **tags** set
    - **develop**: branch where feature or **merge requests** are merged
- **Auxiliary Branches**
    - feature: branch from develop to add new functionalities or significant changes
    - hotfix: branch from master to fix minor bugs

To **contribute** adding or customizing any of the Data Models in this repository, you can **fork** it to make changes
and launch a **merge request** on the develop branch. The team in charge will review and accept the merge request as 
well as set a new tag or merge to master branch.

## Step by step

1. **Fork** the repository
3. **Clone** the repository
4. Create a new branch from **develop**
5. Add a new Smart Data Model
   1. Duplicate the most similar data model, for example [_AirQualityObserved_](../airquality)
   2. Set the new name, for example, _AirQualityForecast_
   3. Add the new attributes, values and metadata
   4. Reference it in [datamodels.md](datamodels.md)
6. Add changes and **commit**
7. **Push** your branch
8. Create a new **Merge Request** to **develop**
9. Review Merge Request until it is approved and **merged** 